cmake_minimum_required(VERSION 3.16)
project(Concept_as_base_class
LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Encourage user to specify a build type (e.g. Release, Debug, etc.), otherwise set it to Release.
if(NOT CMAKE_CONFIGURATION_TYPES)
    if(NOT CMAKE_BUILD_TYPE)
        message(STATUS "Setting build type to 'Release' as none was specified.")
        set_property(CACHE CMAKE_BUILD_TYPE PROPERTY VALUE "Release")
    endif()
endif()

include(cmake/Conan.cmake)
run_conan()

# Defines the CMAKE_INSTALL_LIBDIR, CMAKE_INSTALL_BINDIR and many other useful macros.
# See https://cmake.org/cmake/help/latest/module/GNUInstallDirs.html
include(GNUInstallDirs) 

# Add integration tests (unit tests for each library should be in each sublibrary directory).
option(BUILD_TESTING "Create tests using CMake" ON)
if(BUILD_TESTING)
    enable_testing()
    add_subdirectory(test)
endif()

add_subdirectory(src)