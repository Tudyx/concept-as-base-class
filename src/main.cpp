#include <fmt/core.h>
#include <variant>
#include <vector>

// Declaring some concept
template <typename T>
concept Drawable = requires(T t)
{
    t.draw();
};

template <typename T>
concept perimeterable = requires(T t)
{
    t.perimeter();
};

template <typename T>
concept shapeConcept = requires(T t)
{
    t.perimeter();
    t.draw();
};

class Circle
{
public:
    explicit Circle(double rad) : radius{rad} {}
    void draw() const { fmt::print("Draw circle\n"); }
    void perimeter() const {}

private:
    double radius;
};
static_assert(shapeConcept<Circle>);

class Square
{
public:
    explicit Square(double s) : side{s} {}
    void draw() const { fmt::print("Draw square\n"); }

private:
    double side;
};
static_assert(Drawable<Square>);

class Triangle
{
public:
    explicit Triangle(double s) : side{s} {}
    void draw() const { fmt::print("Draw Triangle\n"); }

private:
    double side;
};
static_assert(Drawable<Triangle>);

// This syntax require clang12
void common(const Drawable auto& shape)
{
    fmt::print("I am a shape\n");
}

using Shape = std::variant<Circle, Square, Triangle>;
int main()
{
    // vector of polymorphic objects (No need to store pointer to the base class)
    std::vector<Shape> shapes = {Circle{1.0}, Square{1.0}, Triangle{1.0}};

    // polymorphic call to draw function
    for (const auto& variant : shapes)
        std::visit([](const auto& shape) { shape.draw(); }, variant);

    // Function can also have the same behavior for all type that respect the concept
    for (const auto& variant : shapes)
        std::visit([](const auto& shape) { common(shape); }, variant);
}
